use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day9)]
fn parse(input: &str) -> Vec<History> {
    input
        .lines()
        .map(|line| {
            let values = line
                .split_ascii_whitespace()
                .map(|c| c.parse().unwrap())
                .collect();

            History { values }
        })
        .collect()
}

struct History {
    values: Vec<i32>,
}

impl History {
    pub fn derive(&self, reversed: bool) -> Vec<Vec<i32>> {
        let mut derived = vec![self.values.clone()];

        while !derived.last().unwrap().iter().all(|x| *x == 0) {
            let last = derived.last().unwrap();
            let mut next = vec![];

            for i in 1..last.len() {
                if reversed {
                    next.push(last[i - 1] - last[i]);
                } else {
                    next.push(last[i] - last[i - 1]);
                }
            }

            derived.push(next);
        }

        derived
    }
}

#[aoc(day9, part1)]
fn part1(input: &[History]) -> i32 {
    input
        .iter()
        .map(|h| h.derive(false).iter().filter_map(|v| v.last()).sum::<i32>())
        .sum()
}

#[aoc(day9, part2)]
fn part2(input: &[History]) -> i32 {
    input
        .iter()
        .map(|h| {
            h.derive(true)
                .iter()
                .rev()
                .filter_map(|v| v.first())
                .sum::<i32>()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 114);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE1)), 2);
    }
}
