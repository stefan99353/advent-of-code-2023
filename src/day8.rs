use aoc_runner_derive::{aoc, aoc_generator};
use num::integer::lcm;
use std::collections::HashMap;

#[aoc_generator(day8)]
fn parse(input: &str) -> Map {
    let input = input.replace('\r', "");
    let (instructions, network) = input.split_once("\n\n").unwrap();

    let instructions = instructions.chars().map(Direction::from).collect();

    let network = network
        .lines()
        .map(|line| {
            let (key, left_right) = line.split_once(" = ").unwrap();
            let left_right = left_right.replace(['(', ')'], "");
            let (left, right) = left_right.split_once(", ").unwrap();

            (key.to_string(), (left.to_string(), right.to_string()))
        })
        .collect();

    Map {
        instructions,
        network,
    }
}

#[derive(Copy, Clone, Debug)]
enum Direction {
    Left,
    Right,
}

impl From<char> for Direction {
    fn from(value: char) -> Self {
        match value {
            'L' => Self::Left,
            'R' => Self::Right,
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, Debug)]
struct Map {
    instructions: Vec<Direction>,
    network: HashMap<String, (String, String)>,
}

#[aoc(day8, part1)]
fn part1(input: &Map) -> u32 {
    let start = "AAA".to_string();
    let mut steps = 0;
    let mut current_position = &start;

    while current_position != "ZZZ" {
        for instruction in &input.instructions {
            current_position = match *instruction {
                Direction::Left => &input.network.get(current_position).unwrap().0,
                Direction::Right => &input.network.get(current_position).unwrap().1,
            };

            steps += 1;
        }
    }

    steps
}

#[aoc(day8, part2)]
fn part2(input: &Map) -> u64 {
    let mut all_steps = vec![];
    let start_positions = input
        .network
        .keys()
        .filter(|x| x.ends_with('A'))
        .collect::<Vec<_>>();

    for start_position in start_positions {
        let mut current = start_position;
        let mut index = 0;
        let mut steps = 0;

        while !current.ends_with('Z') {
            current = match input.instructions[index] {
                Direction::Left => &input.network.get(current).unwrap().0,
                Direction::Right => &input.network.get(current).unwrap().1,
            };

            index = (index + 1) % input.instructions.len();
            steps += 1;
        }

        all_steps.push(steps);
    }

    all_steps.iter().fold(1, |acc, s| lcm(acc, *s))
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)"#;

    const EXAMPLE2: &str = r#"LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 2);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE2)), 6);
    }
}
