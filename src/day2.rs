use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day2)]
fn parse(input: &str) -> Vec<Game> {
    input
        .lines()
        .map(|line| {
            let (game_part, draw_part) = line.split_once(": ").unwrap();

            let game_id = game_part.split_once(' ').unwrap().1.parse::<u32>().unwrap();

            let mut draws = vec![];

            for draw_str in draw_part.split("; ") {
                let mut draw = Draw::default();

                for cube in draw_str.split(", ") {
                    let (num, color) = cube.split_once(' ').unwrap();

                    match color {
                        "red" => draw.red = num.parse().unwrap(),
                        "green" => draw.green = num.parse().unwrap(),
                        "blue" => draw.blue = num.parse().unwrap(),
                        _ => {}
                    }
                }

                draws.push(draw);
            }

            Game { id: game_id, draws }
        })
        .collect()
}

#[derive(Clone, Debug)]
struct Game {
    pub id: u32,
    pub draws: Vec<Draw>,
}

#[derive(Clone, Debug, Default)]
struct Draw {
    pub red: u32,
    pub green: u32,
    pub blue: u32,
}

impl Draw {
    fn possible(&self, red: u32, green: u32, blue: u32) -> bool {
        self.red <= red && self.green <= green && self.blue <= blue
    }
}

#[aoc(day2, part1)]
fn part1(input: &[Game]) -> u32 {
    let red = 12;
    let green = 13;
    let blue = 14;

    input
        .iter()
        .filter(|game| !game.draws.iter().any(|d| !d.possible(red, green, blue)))
        .map(|game| game.id)
        .sum()
}

#[aoc(day2, part2)]
fn part2(input: &[Game]) -> u32 {
    input
        .iter()
        .map(|game| {
            let mut min_red = 0;
            let mut min_green = 0;
            let mut min_blue = 0;

            for draw in &game.draws {
                if draw.red >= min_red {
                    min_red = draw.red;
                }
                if draw.green >= min_green {
                    min_green = draw.green;
                }
                if draw.blue >= min_blue {
                    min_blue = draw.blue;
                }
            }

            min_red * min_green * min_blue
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = r#"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE)), 8);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE)), 2286);
    }
}
