use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{HashMap, VecDeque};

#[aoc_generator(day5)]
fn parse(input: &str) -> Almanac {
    let input = input.replace('\r', "");
    let mut parts = input.split("\n\n").collect::<VecDeque<_>>();

    let seeds = parts
        .pop_front()
        .unwrap()
        .split_once(": ")
        .unwrap()
        .1
        .split(' ')
        .map(|s| s.parse().unwrap())
        .collect::<Vec<_>>();

    let mut map_lists = HashMap::new();

    for part in parts {
        let (source_dest, list) = part.split_once(" map:\n").unwrap();
        let (source, dest) = source_dest.split_once("-to-").unwrap();

        let mut map_list = MapList::default();

        for map in list.split('\n') {
            let map = map
                .split(' ')
                .map(|x| x.parse().unwrap())
                .collect::<Vec<_>>();

            map_list.push(SrcDestMap {
                dest_start: map[0],
                src_start: map[1],
                length: map[2],
            })
        }

        // map_list.sort_unstable_by(|a, b| a.src_start.cmp(&b.src_start));

        map_lists.insert((source.to_string(), dest.to_string()), map_list);
    }

    Almanac { seeds, map_lists }
}

#[derive(Clone, Debug)]
struct Almanac {
    seeds: Vec<u32>,
    map_lists: HashMap<(String, String), MapList>,
}

type MapList = Vec<SrcDestMap>;

fn get_dest(map_list: &MapList, src: u32) -> u32 {
    let mut dest = src;

    for map in map_list.iter().rev() {
        if src >= map.src_start && src < map.src_start + map.length {
            let diff = src - map.src_start;
            dest = map.dest_start + diff;
        }
    }

    dest
}

#[derive(Clone, Debug)]
struct SrcDestMap {
    dest_start: u32,
    src_start: u32,
    length: u32,
}

#[aoc(day5, part1)]
fn part1(input: &Almanac) -> u32 {
    let mut locations: Vec<u32> = vec![];

    let src_dests = input
        .map_lists
        .keys()
        .map(|k| k.to_owned())
        .collect::<HashMap<_, _>>();

    for seed in &input.seeds {
        let mut src_name = "seed".to_string();
        let mut src = *seed;

        while let Some(dest) = src_dests.get(&src_name) {
            let map_lists_key = (src_name.clone(), dest.clone());
            let map_list = input.map_lists.get(&map_lists_key).unwrap();

            src_name = dest.clone();
            src = get_dest(map_list, src);
        }

        locations.push(src);
    }

    *locations.iter().min().unwrap()
}

#[aoc(day5, part2)]
fn part2(input: &Almanac) -> u32 {
    let mut almanac = input.clone();

    let mut seeds = vec![];

    for chunk in almanac.seeds.chunks(2) {
        for i in 0..chunk[1] {
            seeds.push(chunk[0] + i);
        }
    }

    dbg!(seeds.len());

    almanac.seeds = seeds;

    part1(&almanac)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 35);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE1)), 46);
    }
}
