use aoc_runner_derive::{aoc, aoc_generator};

const DIGITS: [(&str, u32); 10] = [
    ("0", 0),
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
];
const DIGITS_SPELLED: [(&str, u32); 20] = [
    ("0", 0),
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
    ("zero", 0),
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
];

#[aoc_generator(day1)]
fn parse(input: &str) -> Vec<String> {
    input.lines().map(|line| line.to_string()).collect()
}

#[aoc(day1, part1)]
fn part1(input: &[String]) -> u32 {
    input
        .iter()
        .map(|line| {
            let first = find_digit(line, false, false);
            let last = find_digit(line, true, false);

            first * 10 + last
        })
        .sum()
}

#[aoc(day1, part2)]
fn part2(input: &[String]) -> u32 {
    input
        .iter()
        .map(|line| {
            let first = find_digit(line, false, true);
            let last = find_digit(line, true, true);

            first * 10 + last
        })
        .sum()
}

fn find_digit(line: &str, reversed: bool, spelled: bool) -> u32 {
    let digits = match spelled {
        false => DIGITS.to_vec(),
        true => DIGITS_SPELLED.to_vec(),
    };

    let mut current_pos = None;
    let mut current_digit = None;

    for (digit, val) in digits {
        match reversed {
            true => {
                if let Some(pos) = line.rfind(digit) {
                    if pos >= current_pos.unwrap_or(pos) {
                        current_pos = Some(pos);
                        current_digit = Some(val);
                    }
                }
            }
            false => {
                if let Some(pos) = line.find(digit) {
                    if pos <= current_pos.unwrap_or(pos) {
                        current_pos = Some(pos);
                        current_digit = Some(val);
                    }
                }
            }
        };
    }

    current_digit.unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"#;
    const EXAMPLE2: &str = r#"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 142);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE2)), 281);
    }
}
