use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day6)]
fn parse(input: &str) -> String {
    input.to_string()
}

#[derive(Clone, Debug)]
struct Race {
    time: u64,
    distance: u64,
}

impl Race {
    fn parse(input: &str) -> Vec<Self> {
        let input = input.replace('\r', "");
        let (times, distances) = input.split_once('\n').unwrap();

        let times = times
            .split_ascii_whitespace()
            .skip(1)
            .map(|x| x.parse::<u64>().unwrap())
            .collect::<Vec<_>>();
        let distances = distances
            .split_ascii_whitespace()
            .skip(1)
            .map(|x| x.parse::<u64>().unwrap())
            .collect::<Vec<_>>();

        times
            .into_iter()
            .zip(distances)
            .map(|(time, distance)| Race { time, distance })
            .collect()
    }

    fn possibilities(&self) -> u32 {
        let mut possibilities = 0;

        for charge_time in 1..self.time {
            let drive_time = self.time - charge_time;
            let speed = charge_time;
            let distance = speed * drive_time;

            if distance > self.distance {
                possibilities += 1;
            }
        }

        possibilities
    }
}

#[aoc(day6, part1)]
fn part1(input: &str) -> u32 {
    Race::parse(input)
        .into_iter()
        .map(|race| race.possibilities())
        .product()
}

#[aoc(day6, part2)]
fn part2(input: &str) -> u32 {
    let input = input
        .lines()
        .map(|line| {
            line.split_ascii_whitespace()
                .collect::<Vec<_>>()
                .join("")
                .replace(':', ": ")
        })
        .collect::<Vec<_>>()
        .join("\n");

    dbg!(&input);

    Race::parse(&input)
        .into_iter()
        .map(|race| race.possibilities())
        .product()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"Time:      7  15   30
Distance:  9  40  200"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 288);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE1)), 71503);
    }
}
