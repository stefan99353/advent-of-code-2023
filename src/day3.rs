use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{HashMap, HashSet};

#[aoc_generator(day3)]
fn parse(input: &str) -> Input {
    let chars = input
        .lines()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut symbol_positions = HashSet::new();
    let mut ratios = HashMap::new();

    for (y, line) in chars.iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if !"0123456789.".contains(*c) {
                symbol_positions.insert((x, y));
            }
        }
    }

    let mut gears = vec![];
    for (y, line) in chars.iter().enumerate() {
        let mut pos = None;

        let mut check = |pos, x| {
            if let Some(pos) = pos {
                let [start, end] = [pos, x - 1];
                let value = line[start..=end]
                    .iter()
                    .collect::<String>()
                    .parse()
                    .unwrap();

                let mut part_number = false;
                for nx in (start as isize - 1)..=(end as isize + 1) {
                    for ny in (y as isize - 1)..=(y as isize + 1) {
                        if nx < 0
                            || ny < 0
                            || (nx as usize) >= chars.len()
                            || (ny as usize) >= chars[nx as usize].len()
                        {
                            continue;
                        }

                        let [nx, ny] = [nx as usize, ny as usize];
                        let pos = (nx, ny);
                        part_number |= symbol_positions.contains(&pos);

                        if symbol_positions.contains(&pos) && chars[ny][nx] == '*' {
                            ratios.entry(pos).or_insert(Vec::new()).push(value);
                        }
                    }
                }

                gears.push(Part {
                    part_number: value,
                    valid_part_number: part_number,
                });
            }
        };

        let mut x = 0;
        while x < line.len() {
            if line[x].is_numeric() {
                if pos.is_none() {
                    pos = Some(x)
                }
            } else {
                check(pos, x);
                pos = None;
            }

            x += 1;
        }

        check(pos, x);
    }

    Input {
        parts: gears,
        ratios,
    }
}

#[derive(Clone, Debug)]
struct Input {
    parts: Vec<Part>,
    ratios: HashMap<(usize, usize), Vec<u32>>,
}

#[derive(Clone, Debug)]
struct Part {
    part_number: u32,
    valid_part_number: bool,
}

#[aoc(day3, part1)]
fn part1(input: &Input) -> u32 {
    input
        .parts
        .iter()
        .filter(|gear| gear.valid_part_number)
        .map(|gear| gear.part_number)
        .sum()
}

#[aoc(day3, part2)]
fn part2(input: &Input) -> u32 {
    input
        .ratios
        .iter()
        .filter(|(_, vals)| vals.len() == 2)
        .map(|(_, vals)| vals.iter().product::<u32>())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 4361);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE1)), 467835);
    }
}
