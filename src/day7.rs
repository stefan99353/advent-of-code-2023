use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::ops::Deref;

#[aoc_generator(day7)]
fn parse(input: &str) -> Vec<Hand> {
    input
        .lines()
        .map(|line| {
            let (cards, bid) = line.split_once(' ').unwrap();
            let cards = cards.chars().map(Card::from).collect();

            Hand {
                cards,
                bid: bid.parse().unwrap(),
            }
        })
        .collect()
}

#[derive(Clone, Debug)]
struct Hand {
    cards: Vec<Card>,
    bid: u32,
}

impl Hand {
    pub fn type_(&self) -> HandType {
        HandType::from(self.cards.as_ref())
    }

    pub fn use_jokers(&mut self) {
        for card in &mut self.cards {
            if card == &Card::Jack {
                *card = Card::Joker
            }
        }
    }
}

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
enum HandType {
    HighCard = 1,
    OnePair = 2,
    TwoPairs = 3,
    ThreeOfAKind = 4,
    FullHouse = 5,
    FourOfAKind = 6,
    FiveOfAKind = 7,
}

impl HandType {
    fn apply_jokers(self, num: u8) -> Self {
        match (self, num) {
            (initial, 0) => initial,
            (Self::FiveOfAKind, _) => Self::FiveOfAKind,

            (Self::HighCard, 1) => Self::OnePair,
            (Self::OnePair, 1) => Self::ThreeOfAKind,
            (Self::TwoPairs, 1) => Self::FullHouse,
            (Self::ThreeOfAKind, 1) => Self::FourOfAKind,
            (Self::FourOfAKind, 1) => Self::FiveOfAKind,

            (Self::OnePair, 2) => Self::ThreeOfAKind,
            (Self::TwoPairs, 2) => Self::FourOfAKind,
            (Self::ThreeOfAKind, 2) => Self::FiveOfAKind,
            (Self::FullHouse, 2) => Self::FiveOfAKind,

            (Self::ThreeOfAKind, 3) => Self::FourOfAKind,
            (Self::FullHouse, 3) => Self::FiveOfAKind,

            (Self::FourOfAKind, 4) => Self::FiveOfAKind,

            _ => unreachable!(),
        }
    }
}

impl From<&[Card]> for HandType {
    fn from(value: &[Card]) -> Self {
        let hand_type = value.iter().counts().values().sorted().join("");

        match hand_type.deref() {
            "11111" => Self::HighCard,
            "1112" => Self::OnePair,
            "122" => Self::TwoPairs,
            "113" => Self::ThreeOfAKind,
            "23" => Self::FullHouse,
            "14" => Self::FourOfAKind,
            "5" => Self::FiveOfAKind,
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
enum Card {
    Joker = 0,
    Two = 1,
    Three = 2,
    Four = 3,
    Five = 4,
    Six = 5,
    Seven = 6,
    Eight = 7,
    Nine = 8,
    Ten = 9,
    Jack = 10,
    Queen = 11,
    King = 12,
    Ace = 13,
}

impl From<char> for Card {
    fn from(value: char) -> Self {
        match value {
            '2' => Self::Two,
            '3' => Self::Three,
            '4' => Self::Four,
            '5' => Self::Five,
            '6' => Self::Six,
            '7' => Self::Seven,
            '8' => Self::Eight,
            '9' => Self::Nine,
            'T' => Self::Ten,
            'J' => Self::Jack,
            'Q' => Self::Queen,
            'K' => Self::King,
            'A' => Self::Ace,
            _ => unreachable!(),
        }
    }
}

#[aoc(day7, part1)]
fn part1(input: &[Hand]) -> u32 {
    input
        .iter()
        .sorted_by_key(|h| (h.type_(), &h.cards))
        .enumerate()
        .map(|(i, hand)| hand.bid * (i + 1) as u32)
        .sum()
}

#[aoc(day7, part2)]
fn part2(input: &[Hand]) -> u32 {
    input
        .iter()
        .map(|hand| {
            let mut hand = hand.clone();

            hand.use_jokers();
            let joker_count = hand.cards.iter().filter(|c| **c == Card::Joker).count() as u8;
            let type_ = hand.type_().apply_jokers(joker_count);

            (hand, type_)
        })
        .sorted_by_key(|(hand, type_)| (*type_, hand.cards.clone()))
        .enumerate()
        .map(|(i, (hand, _))| hand.bid * (i + 1) as u32)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE1: &str = r#"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"#;

    #[test]
    fn part1_example() {
        assert_eq!(part1(&parse(EXAMPLE1)), 6440);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(&parse(EXAMPLE1)), 5905);
    }
}
